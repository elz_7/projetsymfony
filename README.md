#Notes 

git checkout -b avec_doctrine
#Dans cette la partie suivante, nous allons créer notre entité, faire la migration ensuite créer des fixtures pour la base de donnée
#nous allons aussi nous servir de la commande CRUD pour générer des fichiers
#Créer une base de donnée :
php bin/console doctrine:database:create
#Créer une nouvelle enité avec la commande :
php bin/console make:entity
#Créer la table 
php bin/console make:migration
#Créer une base de donnée 
php bin/console doctrine:database:create
#Créer une fixture lancer la commande:
php bin/console make:migrations:migrate #ensuite modifier le fichier créé
#commande fournissant la "DoctrineFixturesBundle" est installé  comme suit:
composer require doctrine/doctrine-fixtures-bundle --dev
#Charger les données dans la table:
php bin/console doctrine:fixtures:load
#Créer un controlleur :
php bin/console make:controller
#Pour générer des fichiers de formulaire, lancer la commande:
php bin/console make:crud NomEntite
#Commande permettant d'installer le serveur en mode dévellopement:
composer require server --dev
#Pour lancer le serveur ensuite, il suffit de faire:
php bin/console server:run
#Pour supprimer les attributs dans la base de sonnée:
php bin/console doctrine:query:sql "ligne de manipulation. Ex:" "ALTER TABLE nomTable DROP COLUMN nomTable.NomColonne"
#Mettre à jour notre Entité, nos fichiers du templates, le fichier dans Form
#Pour changer le type de notre dernier attribut:
php bin/console doctrine:query:sql "ALTER TABLE nomTable CHANGE nomTable.nomColonne nouveauNomColonne typeColonne not null"
#Mettre à jour le type de l'attribut en question dans la classe du package Entity


#FICHIER DOC sur les services

#Se rendre dans le nomController.php
Modifier la fonction index.php, rajouter un Repository puis remplacer le tableau par: $repository->findAll()
Autowiring: le container devine ce dont on a besoin grâce aux paramètre de la fonction.
Mission du container: construire des instances, des objets à partir des classes qu'il connait ! Le container est donc une boîte qui
contient des services.
- php bin/console debug:autowiring
Autowiring:Methode qui nous permet de recevoir directement nos paramètres dans nos fonctions
Nos dossiers Entity et Migrations ne font pas partir des services du container donc ne seront pas inecté dans le container
installer avec le composer notre Markdown avec la commande :
composer require cebe/markdown
Ensuite modifier la Classe Controller pour rajouter un parser de type Markdown, mais surtout préciser le nouveau service
dans notre dossier config/dans le fichier services.yaml qui est aussi une Classe

